package ito.poo.app;
import ito.poo.clases.Aplicacion;
import ito.poo.clases.ExcepcionFechaRegreso;

public class MyApp  {
	public static void main(String[] args) throws ExcepcionFechaRegreso {
		try {
			Aplicacion.run();
			}catch (ExcepcionFechaRegreso e) {
				System.err.println(e.getMessage());
				}
		}
}