package ito.poo.clases;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import ito.dsc.input.FormInput;
import ito.dsc.output.FormOutput;

public class Aplicacion implements Serializable  {

	static private ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
	static private ArrayList<Viajes> viajes = new ArrayList<Viajes>();
	static private FormInput menu = new FormInput();
	static private final int FIN = 7;

	static void generaMenu() {
		menu.addItemMenu("1.- Agregar Vehiculo");
		menu.addItemMenu("2.- Lista de Vehiculos");
		menu.addItemMenu("3.- Eliminar Vehiculo");
		menu.addItemMenu("4.- Cancelar Viaje");
		menu.addItemMenu("5.- Asignar Viaje a Vehiculo");
		menu.addItemMenu("6.- Viajes asignados a vehiculos");
		menu.addItemMenu("7.- Salir");
	}

	static String capturaRegistroMarca() {
		String registromarca;
		String txregistromarca = FormInput.leeString("Marca del vehiculo");
		registromarca = String.format(txregistromarca);
		return registromarca;
	}

	static String capturamodelo() {
		String capturamodelo;
		String txcapturamodelo = FormInput.leeString("Modelo");
		capturamodelo = String.format(txcapturamodelo);
		return capturamodelo;
	}

	static float capturacantidadMaxCarga() {
		float cantidadMaxCarga;
		float txcantidadMaxCarga = FormInput.leeFloat("Cantidad Maxima de carga");
		cantidadMaxCarga = Float.floatToIntBits(txcantidadMaxCarga);
		return cantidadMaxCarga;
	}

	static LocalDate capturafechaAdquisicion() {
		LocalDate fechaAdquisicion;
		String txtcapturafechaAdquisicion = FormInput.leeString("Proporciona fecha de cita:[aaaa-mm-dd]:");
		fechaAdquisicion = LocalDate.parse(txtcapturafechaAdquisicion);
		return fechaAdquisicion;
	}

	static Vehiculo AgregarVehiculo() {
		String registromarca = capturaRegistroMarca();
		String capturamodelo = capturamodelo();
		float cantidadMaxCarga = capturacantidadMaxCarga();
		LocalDate fechaAdquisicion = capturafechaAdquisicion();
		return new Vehiculo(registromarca, capturamodelo, cantidadMaxCarga, fechaAdquisicion, viajes, 0);
	}

	static void addVehiculo() {
		Vehiculo vehiculo = AgregarVehiculo();
		vehiculos.add(vehiculo);
	}

	static void listVehiculos() {
		FormOutput.imprimeListaTabla(vehiculos, "Lista de citas");
	}

	static void EliminarVehiculo() {
		for (Vehiculo vehiculo : vehiculos)
			if (FormInput.leeBoolean(vehiculo + "\nEs el Viaje a eliminar:")) {
				viajes.remove(vehiculo);
				break;
			}
	}

	/***/
	static String capturaciudadDestino() {
		String ciudadDestino;
		String txciudadDestino = FormInput.leeString("Ciudad Destino");
		ciudadDestino = String.format(txciudadDestino);
		return ciudadDestino;
	}

	static String capturadireccion() {
		String direccion;
		String txdireccion = FormInput.leeString("Direccion");
		direccion = String.format(txdireccion);
		return direccion;
	}

	static LocalDate capturaFechaViaje() {
		LocalDate FechaViaje;
		String txtFechaViaje = FormInput.leeString("Proporciona fechaViaje:[aaaa-mm-dd]:");
		FechaViaje = LocalDate.parse(txtFechaViaje);
		return FechaViaje;
	}

	static LocalDate capturaFechaRegreso() {
		LocalDate FechaRegreso;
		String txtFechaRegreso = FormInput.leeString("Proporciona fecha Regreso:[aaaa-mm-dd]:");
		FechaRegreso = LocalDate.parse(txtFechaRegreso);
		return FechaRegreso;
	}

	static String capturadescripcionCarga() {
		String descripcionCarga;
		String txdescripcionCarga = FormInput.leeString("Descripcion Carga");
		descripcionCarga = String.format(txdescripcionCarga);
		return descripcionCarga;
	}

	static String capturamontoViaje() {
		String montoViaje;
		String txmontoViaje = FormInput.leeString("MontoViaje");
		montoViaje = String.format(txmontoViaje);
		return montoViaje;
	}

	static Viajes Viajes() throws ExcepcionFechaRegreso{
		String ciudadDestino = capturaciudadDestino();
		String direccion = capturadireccion();
		LocalDate FechaViaje = capturaFechaViaje();
		LocalDate FechaRegreso = capturaFechaRegreso();
		String descripcionCarga = capturadescripcionCarga();
		String montoViaje = capturamontoViaje();
		return new Viajes(ciudadDestino, direccion, FechaViaje, FechaRegreso, descripcionCarga, montoViaje);
	}

	static void addAsignarViaje()throws ExcepcionFechaRegreso {
		Viajes viaje = Viajes();
		viajes.add(viaje);
		for (Viajes viajes : viajes)
			if (FormInput.leeBoolean(vehiculos + "\nDesea este vehiculo para su viaje:")) {
				vehiculos.contains(viajes);

			}
	}

	static void CancelarViaje() {
		for (Viajes viaje : viajes)
			if (FormInput.leeBoolean(viaje + "\nEs el Viaje a eliminar:")) {
				viajes.remove(viaje);
				break;
			}
	}
	static void ViajesAsignados() {
		FormOutput.imprimeListaTabla(viajes, "viajes asignados");
		FormOutput.imprimeListaTabla(vehiculos, "vehiculos");
				
			}
	public static void run() throws ExcepcionFechaRegreso {
		generaMenu();
		vehiculos=Persistencia.recuperaVehiculo();
		int opc;
		do {
			opc = menu.menuOption();
			switch (opc) {
			case 1:
				addVehiculo();
				break;
			case 2:
				listVehiculos();
				break;
			case 3:
				EliminarVehiculo();
				break;
			case 4:
				CancelarViaje();
				break;
			case 5:
				addAsignarViaje();
				break;
			case 6:
				ViajesAsignados();
				break;
				}
			} while (opc != FIN);
		Persistencia.grabaVehiculos(vehiculos);
	}
}