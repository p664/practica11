package ito.poo.clases;

public class ExcepcionFechaRegreso extends Exception {
	private static final long serialVersionUID = 1L;

	public ExcepcionFechaRegreso(String msg) {
		super(msg);
	}
}