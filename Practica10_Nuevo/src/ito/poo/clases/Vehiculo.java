package ito.poo.clases;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;

public class Vehiculo implements Comparable<Vehiculo>, Serializable {
	private String registroMarca;
	private String modelo;
	private float cantidadMaxCarga;
	private java.time.LocalDate fechaAdquisicion;
	private ArrayList<Viajes> listaViajesRegistrados= new ArrayList<Viajes>();
	private int identificador;

	public float asignarVehiculo() {
		return cantidadMaxCarga;
	}
	public float eliminarVehiculo() {
		return cantidadMaxCarga;
	}
	Vehiculo(String registroMarca, String modelo, float cantidadMaxCarga, LocalDate fechaAdquisicion, ArrayList<Viajes> listaViajesRegistrados, int identificador) {
		super();
		this.registroMarca = registroMarca;
		this.modelo = modelo;
		this.cantidadMaxCarga = cantidadMaxCarga;
		this.fechaAdquisicion = fechaAdquisicion;
		this.listaViajesRegistrados = listaViajesRegistrados;
		this.identificador = identificador;
	}
	@Override
	public String toString() {
		return "Vehiculo [registroMarca=" + registroMarca + ", modelo=" + modelo + ", cantidadMaxCarga="
				+ cantidadMaxCarga + ", fechaAdquisicion=" + fechaAdquisicion + ", listaViajesRegistrados="
				+ listaViajesRegistrados + ", identificador=" + identificador + "]";
	}
	public String getRegistroMarca() {
		return registroMarca;
	}
	public void setRegistroMarca(String registroMarca) {
		this.registroMarca = registroMarca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public float getCantidadMaxCarga() {
		return cantidadMaxCarga;
	}
	public void setCantidadMaxCarga(float cantidadMaxCarga) {
		this.cantidadMaxCarga = cantidadMaxCarga;
	}
	public java.time.LocalDate getFechaAdquisicion() {
		return fechaAdquisicion;
	}
	public void setFechaAdquisicion(java.time.LocalDate fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}
	public ArrayList<Viajes> getListaViajesRegistrados() {
		return listaViajesRegistrados;
	}
	public void setListaViajesRegistrados(ArrayList<Viajes> listaViajesRegistrados) {
		this.listaViajesRegistrados = listaViajesRegistrados;
	}
	public int getIdentificador() {
		return identificador;
	}
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(cantidadMaxCarga);
		result = prime * result + ((fechaAdquisicion == null) ? 0 : fechaAdquisicion.hashCode());
		result = prime * result + identificador;
		result = prime * result + ((listaViajesRegistrados == null) ? 0 : listaViajesRegistrados.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((registroMarca == null) ? 0 : registroMarca.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehiculo other = (Vehiculo) obj;
		if (Float.floatToIntBits(cantidadMaxCarga) != Float.floatToIntBits(other.cantidadMaxCarga))
			return false;
		if (fechaAdquisicion == null) {
			if (other.fechaAdquisicion != null)
				return false;
		} else if (!fechaAdquisicion.equals(other.fechaAdquisicion))
			return false;
		if (identificador != other.identificador)
			return false;
		if (listaViajesRegistrados == null) {
			if (other.listaViajesRegistrados != null)
				return false;
		} else if (!listaViajesRegistrados.equals(other.listaViajesRegistrados))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (registroMarca == null) {
			if (other.registroMarca != null)
				return false;
		} else if (!registroMarca.equals(other.registroMarca))
			return false;
		return true;
	}
	public int compareTo(Vehiculo arg0) {
		int r=0;
		if (!this.registroMarca.equals(arg0.getRegistroMarca()))
			return this.registroMarca.compareTo(arg0.getRegistroMarca());
		else if (!this.modelo.equals(arg0.getModelo()))
			return this.modelo.compareTo(arg0.getModelo());
		else if(this.cantidadMaxCarga != arg0.getCantidadMaxCarga())
			return this.cantidadMaxCarga>arg0.getCantidadMaxCarga()?1:-1;
		else if (!this.fechaAdquisicion.equals(arg0.getFechaAdquisicion()))
			return this.fechaAdquisicion.compareTo(arg0.getFechaAdquisicion());
		else if(this.identificador != arg0.getIdentificador())
			return this.identificador>arg0.getIdentificador()?1:-1;
		return r;
	}
	public Object getDescripcion() {
		return null;
	}
	public Object getMemoria() {
		return null;
	}
	public Object getCosto() {
		return null;
	}
}