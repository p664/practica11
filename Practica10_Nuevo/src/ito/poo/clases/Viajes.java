package ito.poo.clases;
import java.io.Serializable;
import java.time.LocalDate;

public class Viajes implements Comparable <Viajes>, Serializable  {

	private String ciudadDestino;
	private String direccion;
	private java.time.LocalDate fechaViaje;
	private java.time.LocalDate fechaRegreso;
	private String descripcionCarga;
	private String montoViaje;

	
	Viajes(String ciudadDestino, String direccion, LocalDate fechaViaje, LocalDate fechaRegreso,
			String descripcionCarga, String montoViaje) throws ExcepcionFechaRegreso {
		super();
		this.ciudadDestino = ciudadDestino;
		this.direccion = direccion;
		this.fechaViaje = fechaViaje;
		this.setFechaRegreso(fechaRegreso);
		this.descripcionCarga = descripcionCarga;
		this.montoViaje = montoViaje;
	}
	@Override
	public String toString() {
		return "Viajes [ciudadDestino=" + ciudadDestino + ", direccion=" + direccion + ", fechaViaje=" + fechaViaje
				+ ", fechaRegreso=" + fechaRegreso + ", descripcionCarga=" + descripcionCarga + ", montoViaje="
				+ montoViaje + "]";
	}
	public String getCiudadDestino() {
		return ciudadDestino;
	}
	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public java.time.LocalDate getFechaViaje() {
		return fechaViaje;
	}
	public void setFechaViaje(java.time.LocalDate fechaViaje){
			this.fechaViaje = fechaViaje;
	}
		
	public java.time.LocalDate getFechaRegreso() {
		return fechaRegreso;
	}
	public void setFechaRegreso(java.time.LocalDate fechaRegreso) throws ExcepcionFechaRegreso {
		if(fechaRegreso.compareTo(this.fechaViaje)>=0)
		this.fechaRegreso = fechaRegreso;
		else
			throw new ExcepcionFechaRegreso("NO SE PUEDE ASIGNAR EL VIAJE");
	}
	
	public String getDescripcionCarga() {
		return descripcionCarga;
	}
	public void setDescripcionCarga(String descripcionCarga) {
		this.descripcionCarga = descripcionCarga;
	}
	public String getMontoViaje() {
		return montoViaje;
	}
	public void setMontoViaje(String montoViaje) {
		this.montoViaje = montoViaje;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ciudadDestino == null) ? 0 : ciudadDestino.hashCode());
		result = prime * result + ((descripcionCarga == null) ? 0 : descripcionCarga.hashCode());
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((fechaRegreso == null) ? 0 : fechaRegreso.hashCode());
		result = prime * result + ((fechaViaje == null) ? 0 : fechaViaje.hashCode());
		result = prime * result + ((montoViaje == null) ? 0 : montoViaje.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Viajes other = (Viajes) obj;
		if (ciudadDestino == null) {
			if (other.ciudadDestino != null)
				return false;
		} else if (!ciudadDestino.equals(other.ciudadDestino))
			return false;
		if (descripcionCarga == null) {
			if (other.descripcionCarga != null)
				return false;
		} else if (!descripcionCarga.equals(other.descripcionCarga))
			return false;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (fechaRegreso == null) {
			if (other.fechaRegreso != null)
				return false;
		} else if (!fechaRegreso.equals(other.fechaRegreso))
			return false;
		if (fechaViaje == null) {
			if (other.fechaViaje != null)
				return false;
		} else if (!fechaViaje.equals(other.fechaViaje))
			return false;
		if (montoViaje == null) {
			if (other.montoViaje != null)
				return false;
		} else if (!montoViaje.equals(other.montoViaje))
			return false;
		return true;
	}
	public int compareTo(Viajes arg0) {
		int r=0;
		if (!this.ciudadDestino.equals(arg0.getCiudadDestino()))
			return this.ciudadDestino.compareTo(arg0.getCiudadDestino());
		else if (!this.direccion.equals(arg0.getDireccion()))
			return this.direccion.compareTo(arg0.getDireccion());
		else if (!this.fechaViaje.equals(arg0.getFechaViaje()))
			return this.fechaViaje.compareTo(arg0.getFechaViaje());
		else if (!this.fechaRegreso.equals(arg0.getFechaRegreso()))
			return this.fechaRegreso.compareTo(arg0.getFechaRegreso());
		else if (!this.descripcionCarga.equals(arg0.getDescripcionCarga()))
			return this.descripcionCarga.compareTo(arg0.getDescripcionCarga());
		else if (!this.montoViaje.equals(arg0.getMontoViaje()))
			return this.montoViaje.compareTo(arg0.getMontoViaje());
		return r;
	}
}